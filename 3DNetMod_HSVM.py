from read_settings import read_settings
from load_community_dict import load_community_dict
from load_region_start_end_coord import load_region_start_end_coord
from create_tags import create_tags
from hierarchical_spatial_variance_minimization import HSVM
import sys 
import glob
import os
from remove_chaos_communities import remove_chaos_communities
from write_consistent_communities import write_consistent_communities
import subprocess
import numpy as np

def main():

     settings = read_settings(sys.argv[1])

     illegals = set('_. ,')
     for key in settings:
         if (key[:-2:] == 'sample') or key == 'sample':
             if  any((c in illegals) for c in settings[key]):
                 print "sample name not formatted correctly (i.e. should not have underscores, periods, commas)"
                 return 0    


     tags = create_tags(sys.argv[1])
     tags_HSVM = tags[3]
     tags_MMCP = tags[2]
     tags_GPS = tags[1]
     tags_preprocess = tags[0]

     dir = 'output/HSVM/output/HSVM/variance_thresholded_communities/merged'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*' 
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True) 

     dir = 'output/HSVM/output/HSVM/variance_thresholded_communities/unmerged'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*'
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True)

    
     dir = 'output/HSVM/variance_thresholds'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*' 
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True)

     dir = 'output/HSVM/variance_distributions'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*' 
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True)

     dir = 'output/HSVM/chaos_filtered_communities'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*' 
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True)
     

     dir = 'output/HSVM/size_hist/'
     if not os.path.isdir(dir): os.makedirs(dir)
     if len(glob.glob(dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*')) > 0:
         file_removal = dir + '/*' + settings['sample_1'] + '*' + tags_HSVM +  '*'
         command_removal = 'rm ' + file_removal
         print command_removal
         subprocess.call(command_removal,shell=True)    


     samples= []
     for sample_inst in settings:
         if sample_inst[:7] == 'sample_':
             samples.append(settings[sample_inst])
     samples = sorted(samples)
     for sample in samples:
         pre_threshold_file = 'Communities_*' + sample + '*_' + tags_MMCP + '.txt'  #test to make sure unique communities are present

         dir = 'output/MMCP/unique_communities/results_files/'
         if  not os.path.isdir(dir):
             print "No communities directory present"
             return 0
         existence = glob.glob(dir + pre_threshold_file)

         if len(existence) == 0:
             pre_threshold_file = 'Communities_*' + sample + '*_' + tags_MMCP + '.txt'
             pre_threshold_file = pre_threshold_file.replace('_' + 'genomewide', '_' + 'chr*')            
             existence = glob.glob(dir + pre_threshold_file)
             if len(existence) == 0:
                 print "No intermediate file present. exiting"
                 return 0

         master_community_dict = {}
         existence.sort() #sort subchrom regions
  
         last_length = 0 
         prevent_inf_loop = 0 
         while last_length < 3:  #ensure last community list is not empty
             last_length = len(np.genfromtxt(existence[-1]))
             backend = existence.pop()
             existence.insert(0,backend)
             if prevent_inf_loop > len(existence):
                 break
             prevent_inf_loop = prevent_inf_loop + 1 

         for existing_file in existence:
             cell_type = existing_file.split('/')[-1].split('_')[1] ### want this to not include chr!
             chr = cell_type.split('.')[0]
             sub_chrom = cell_type.split(sample)[0]
             if chr not in master_community_dict:
                    master_community_dict[chr] = {}
             cell_type_input = cell_type.replace(sample,'') + "_" + sample
             community_dict = load_community_dict(existing_file)
             if len(community_dict) == 0:
                 print "file empty"
             else:
                 master_community_dict[chr][sub_chrom] = community_dict
             
         for chr in master_community_dict:
             dir2 = 'output/HSVM/variance_thresholded_communities/results_files/merged/'
             final_output = 'Merged_' + 'Communities_' + chr + cell_type  + tags_HSVM + '.txt'
             existence = glob.glob(dir2 + final_output)
             if len(existence) > 0: #already found output based on merged
                 continue
             else:
                 variance_threshold = HSVM(settings,master_community_dict[chr],sample, tags_HSVM,tags_preprocess, chr)

     if settings['chaosfilter'] == 'True':
         print "remove_chaos_communities"
         remove_chaos_communities(settings, tags_GPS, tags_HSVM)

     if 'final_consistent_domains' in settings.keys():
         if settings['final_consistent_domains'] == 'True':
             write_consistent_communities(settings, tags_preprocess, tags_HSVM)
             

if __name__ == "__main__":
      main()                       
