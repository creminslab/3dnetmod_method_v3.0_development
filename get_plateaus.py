import numpy as np


def get_plateaus(avg_num_comm_array, gammas, plateau_size):
	hierarchies = {}
	temp_list = []
	level_blocks = {}

	
	previous_avg_num_comm = avg_num_comm_array[0]
	for i in range(len(gammas)):
		gamma = gammas[i]
		avg_num_comm = avg_num_comm_array[i]
		if avg_num_comm == previous_avg_num_comm:
			temp_list.append(gamma)
		elif avg_num_comm > previous_avg_num_comm:
			if len(temp_list)>=plateau_size:
				hierarchies[previous_avg_num_comm] = temp_list
			temp_list = []
			temp_list.append(gamma)
		if i == len(gammas) - 1:
			if len(temp_list) > 1:
				hierarchies[avg_num_comm] = {}
				hierarchies[avg_num_comm] = temp_list

		previous_avg_num_comm = avg_num_comm
	
	return hierarchies


	
