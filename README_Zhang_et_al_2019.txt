software: https://bitbucket.org/creminslab/3dnetmod_method_v3.0_development

domain calling processing steps plus analysis and panel subset generation steps for figures 1,2,3,4,5 extended S4,S5,S6,S7,S8,S9,S10

Analysis with domain calls and IS are generated in this folder. Plots with PCA and 250kb heatmaps are copied over for plotting.  Chipseq tracks generated and referenced (.bw) from  chipseq folder.  kr qnormed at 10kb for plotting (.npz for each chr) generated and referenced from preprocess_hic. 


Nota Bene:  preferable to run on cluster or cloud service if access available.  In each of the shell scripts a bsub command was added (removed out here although bsub maybe present in some analysis cases in shell scripts) prior to python command.  We ran on LSF cluster.  Also, may run into issues with plotting - we used a lab proprietary plotting software (lib5c) underwritten in matplotlib/python.  So plotting issues with dependencies from modules not existing in this directory.  Otherwise all other modules and raw files present (chipseq .bw, domain calls, kr and kr qnormed plots, PCA bedgraphs as well as their pathways).

0min = prometa
25min = ana/telo
60min = early G1
120min = mid G1
240min = late G1


==============
DOMAIN CALLING
==============

Pipeline is setup so that settings file is used for all steps with select parameters passed in.

Settings file setup (for each chromosome and each timepoint separately)

Step1: there are settings txt files for every chromosome across all five timepoints.  Output qnormed and kr balanced *.txt files ( < 15 MB from diagonal counts) specified in preprocess README used as input and placed in input directory (*min_10000v3_merge.counts and 0min_10000_merge.bed).  Also, chaos_pct is mentioned, however, additional chaos filtered files are not actually used so chaosfilter can be False with chaos_pct parameter removed from settings (just worked with output/HSVM/variance_thresholded_communities/merged/ after HSVM) .  mm9 coordinates

#PRE_PROCESSING#
bed_file    0min_10000_merge.bed
sample_1    mitosis*min10kbmerge
counts_file_1   *min_10000v3_merge.counts
overlap 500 
region_size 750 
logged  True
processors    10  
resolution    10000

#GPS#
badregionfile   mm9_cent_tel
badregionfilter True
diagonal_density    0.65
consecutive_diagonal_zero    50  
scale   chr*
plateau 30  
chaosfilter True
chaos_pct   0.30

#MMCP#
num_part        20  
plots   False 
pctile_threshold        0   
pct_value       0   

#HSVM#
size_threshold    15  
trash_edge    4
variance_type value
size_s1 400000
size_s2 800000
size_s3 1600000
size_s4 3000000
size_s5 12000000
var_thresh1 12
var_thresh2     12  
var_thresh3     12  
var_thresh4     12  
var_thresh5     12  
boundary_buffer 10000

* = chr#(1,2 ....19,X) or timepoint (0,25,60,120,240)


Step2: 3dnetmod preprocess - chunk chr in regions specified by overlap and regionsize.  Input files specified by bed_file and counts_file_1 are in input subdirectory. Output from step 2 is also placed in input subdirectory as superregion block of 3 regions for given chr (chr#.superregion# for both bed and counts).  Example output shown for chr2.  Genomewide CPU time: ~60000 seconds

sh 3DNetMod_preprocess_zhang_et_al.sh


Step3: GPS_MMCP.  Input files are the superregion files from step1.  Output that is generated and used for HSVM is located in output/MMCP/unique_communities/results_files.  Example uses chr2 (actually submitted each of the 5 uncommented in .sh as separate jobs (bsub command for LSF, bsub python 3DNetMod_GPS_MMCP.py ...)).  chr2 CPU time per timepoint: ~15000 seconds.  Genomewide CPU time per timepoint:  ~200000 seconds

output located in output/MMCP/unique_communities/results_files (left all chr files in output folder since memory is small)

sh 3DNetMod_GPS_MMCP_zhang_et_al.sh (also bsub sh 3DNetMod_GPS_MMCP.sh for LSF cluster job)


 
Step4: HSVM.  Input files are the output files for MMCP from Step3. Genomewide CPU time: ~ 200 seconds

output in output/HSVM/variance_thresholded_communities/merged (did not use chaos filter output because reading depth was good)
*v1_12_v2_12_v3_12_v4_12_v5_12_10000.txt

sh 3DNetMod_HSVM_zhang_et_al.sh


WARNING! last steps involving emergent timepoint domains are specific to paper zhang et al. "Chromatin structure dynamics during the mitosis-to-G1 phase transition".  The parameters used in scripts are specific to conditions in paper - 0min, 25min, 60min, 120min, 240min.

Step5: Collect final unadjusted domain calls where call shows up atleast one future timepoint (discard 240 minute because no future timepoint).  Consistancy is defined as call within 6 bins of start and stop of earlier time point comparison or total gap between starts and stops is at most 10% of both domains.  Second refinement step added where domains +/- 4 bins on both upstream and downstream boundaries were merged into single emergent domain for each time instance.  The input files that are used are the merged calls post HSVM (output/HSVM/variance_thresholded_communities/merged).  Other postprocess extraneous files (i.e. chaos filter) found to not be useful (reading depth too high to be useful).

output is located in output/HSVM/variance_thresholded_communities/merged/ and the suffix *twice_refine_LDv3_ex240.txt is appended to file name.  Genomewide CPU time: ~ 2500 seconds

sh classify_domains_compro_refine_240minv2_exclude240min.sh (per chromosome with 240min settings files)
  
Note: MUST use 0min as argument in second position to properly search through complete range of times.  

FINAL DOMAIN + BOUNDARY CALLS:

Step6: Adjust domain calls to same consistant boundary for nesting of domains.  Use Step5 output file as input files.  Adjustment is made by looking at domain boundaries that are within 6 bins of each other or 7.5% domain size of prospective domains.  An averaged boundary is assigned to all calls considered to have same boundary.  Final domain calls are made with suffix *_adjust2.txt and final unique boundary calls are made with suffix *_adjust2_b.txt.   CPU time genomewide = ~ 40 seconds

output is located in output/HSVM/variance_thresholded_communities/merged
*_adjust2.txt appended to name
*_adjust2_b.txt appended to name

sh boundary_adjustmentv2_ex240.sh (final adjusted emergent domains)

Note: pass in 240min as argument in second position plus settingsfile at 240 min.  Gives final emergent calls per timepoint up to 20min.  Repeat 20x for each chromosome at 240min.



Step6 part2:  concatenate chromosome specific domain and boundary calls into genomewide calls per timepoint (ALL CALLS EMERGENT UPTO TIMEPOINT).  Go to the output folder: output/HSVM/variance_thresholded_communities/merged.  Run

sh command_0min.sh
sh command_0min_b.sh
sh command_25min.sh
sh command_25min_b.sh
sh command_60min.sh
sh command_60min_b.sh
sh command_120min.sh
sh command_120min_b.sh
sh command_240min.sh
sh command_240min_b.sh

(Note: because emergent 240min calls discarded, the 240min genomewide calls are identical to 120min).  

For the genomewide domain calls upto timepoint the column format is:  chr#  Start_coordinate   End_coordinate   left_boundary_var    right_boundary_var     emergent_time_up_to_current    number_of_occurences   processed

For the genomewide boundary calls upto timepoint the column format is:  chr#    Boundary_coordinate   emergent_time_up_to_current   

After concatenation header was added for boundary calls (tab separated): #chr    boundary    time_point
 
After concatenation header was added for domain calls (tab separated):  #chr    start   end left_var    right_var   time_point  (necesary for proper file loading of calls during analysis)


Final adjusted calls and boundaries in place in output/HSVM/variance_thresholded_communities/merged (*240min*genomewide*adjust2.txt, *240min*genomewide*adjust2_b.txt)


Notes on IS bedgraph generation (FigureS4AF scripts further below under analysis):
---------------------------------------------------------------------------------

run averaged IS pileup of emergent timepoint boundary call values (+-400 kb of boundaries) at all timepoints (0 minute - 240 minute)  and succesive times using log normalized approach of Crane et al. 2015 with IS from empty zones (<10% IS ROI size) and ROI cutoff at chr beginning discarded. Parameter sweep of 1 bin offset and summation square size of 12,25,50,75,and 100 bins.   Input are the final adjusted boundary calls as well as the kr qnormed .npz sparse matrices for relevant timepoint comparison (input subdirectory with path explicitely given in .sh script).  Note: I used a settings file for specific chr but only to pass in relevant parameters from HSVM - still genomewide.  Also the kr qnormed file specifies chr and time but is switched out from list of chr and times inside insulation_boundary_scorev3_mean.py to make dictionary.

script - insulation_scorev3_boundary_mean.py


run deeptools visualization of emergent timepoint boundaries call values (+-400 kb of boundaries) at emergent timepoint and succesive times using log normalized approach of Crane et al. 2015 with IS from empty zones (<10% IS ROI size) and ROI cutoff at chr beginning discarded.  Parameter sweep of 1 bin offset and summation square size of 12,25,50,75,and 100 bins. Bedgraphs and .bw are generated as intermediaries.  Note: must have deeptools and bedGraphToBigWig.  Line  pathway to bedGraphToBigWig must be modified.  computeMatrix and plotHeatmap on line 135 and 144 from deep tools also used. Input are the final adjusted boundary calls as well as the kr qnormed .npz sparse matrices for relevant timepoint comparison (input subdirectory with path explicitely given in .sh script).  Note: I used a settings file for specific chr but only to pass in relevant parameters from HSVM - still genomewide.  Also the kr qnormed file specifies chr and times but is switched out from list of chr and times inside insulation_scorev3_boundary.py to make dictionary

script - insulation_scorev3_boundary.py



========
ANALYSIS
========

Next steps use lib5c plotter in conjunction with kr or kr+qnormed heatmaps, IS bedGraph, PCA bedGraph, and domain calls

Figure 1BC

plot chromosomewide compartment heatmaps at chr1  at all timepoints with kr counts and evolving PCA and varying colorscale using 250kb counts files in input subdirectory (250kb KR files generated by Blobel lab).  For chromosomewide "-1" used as start coordinate to force chromosomewide.  Plot chr1 87300000-138300000 region of obs/exp from output/obs_exp.  No domain calls overlaid.  PCA copied from Blobel lab (stored in output/PCA/)

output in output/Figure1BC:
PCA_timepoint_bedGraph*_PCA_*chr*.png  (for chr1 87300000-138300000 coordinates in name for all five timepoints, varying color scale given as #.png)
sh plot_chr_merged_cc_PCA_kr_exp.sh
sh plot_chr_merged_cc_PCA.sh

output in output/Figure1BC


Figure 2A

plot region of interest (merged kr+qnorm)  with IS information (lower panels, merged kr + qnorm).  Select 6 MB region of chr 2 at all timepoints with evolving domain calls and IS (12 bin ROI with 1 bin offset).  Input adjusted domains from output/HSVM/variance_thresholded_communities/merged/ are used along with corresponding chr kr qnormed merged timepoint .npz files from preprocess_hic merged timepoint folder and bedGraph IS files (logcorrespond*12_1LDv3.bedGraph) located in output/IS/ (also regenerated bedgraphs in FigureS4AF).

*min_merge_chr2_120minlocus259000000_61500000_*twice_refine_IS_LDv3_ex240_adjust2.png

sh plot_loci_merged_cc_IS.sh

output in output/Figure2A

Figure2DEFG

Get number of emergent domain calls per timepoint and total number of domain calls upto timepoint.  Input are final adjusted domains found in output/HSVM/variance_thresholded_communities/merged/. Get distribution of sizes of emergent domains per timepoint.  Input are final adjusted domains found in output/HSVM/variance_thresholded_communities/merged.

sh boxplot.sh
sh barplot_sizes.sh
Note: barplots and boxplots actually generated by Blobel lab. Just making plots from emergent domain and boundaries to help confirm visualization and numbers. Placed additional copy of final unique boundaries and emergent domains through 240min (*refine_LDv3_ex240_adjust2.txt and refine_LDv3_ex240_adjust2_b.txt) from output/HSVM/variance_thresholded_communities/merged.

output in output/Figure2DEFG


Figure3ABCDJL

plot chr2:167400000-167900000 kr + qnorm merged (also replicate provided) across all timepoints and chr1:50600000-52000000 kr + qnorm merged (also replicate provided) across all timepoints.  I also link to the chipseq .bw (replicate merged across timpoints) and *.narrowPeak and *.broadPeak calls for 0min replicates and merged intermediary call files.  merged .bw from chipseq directory.  Refer to absolute path kr + qnorm merged in shell scripts.

sh plot_chr_merged_loops_submission2_locus1.sh
sh plot_chr_merged_loops_submission2_locus2.sh  

output in output/Figure3ABCDJL

symbolic link to chipseq/CTCF_reps_merge_0min_downsampled_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/CTCF_reps_merge_25min_downsampled_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/CTCF_reps_merge_60min_downsampled_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/CTCF_reps_merge_120min_downsampled_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/CTCF_reps_merge_240min_downsampled_treat_pileup_sorted_clipped.bw

symbolic link to chipseq/Rad21_reps_merge_0min_downsampled_punctate_p1e4_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/Rad21_reps_merge_25min_downsampled_punctate_p1e4_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/Rad21_reps_merge_60min_downsampled_punctate_p1e4_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/Rad21_reps_merge_120min_downsampled_punctate_p1e4_treat_pileup_sorted_clipped.bw
symbolic link to chipseq/Rad21_reps_merge_240min_downsampled_punctate_p1e4_treat_pileup_sorted_clipped.bw

symbolic link to chipseq/CTCF_rep0_0min_downsampled_peaks.narrowPeak 
symbolic link to chipseq/CTCF_rep1_0min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_rep2_0min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_reps_merge_0min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_reps_merge_25min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_reps_merge_60min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_reps_merge_120min_downsampled_peaks.narrowPeak
symbolic link to chipseq/CTCF_reps_merge_240min_downsampled_peaks.narrowPeak

symbolic link to chipseq/Rad21_rep3_0min_downsampled_peaks.broadPeak
symbolic link to chipseq/Rad21_rep4_0min_downsampled_peaks.broadPeak
symbolic link to chipseq/Rad21_reps_merge_0min_downsampled_punctate_p1e4_peaks.narrowPeak
symbolic link to chipseq/Rad21_reps_merge_25min_downsampled_punctate_p1e4_peaks.narrowPeak
symbolic link to chipseq/Rad21_reps_merge_60min_downsampled_punctate_p1e4_peaks.narrowPeak
symbolic link to chipseq/Rad21_reps_merge_120min_downsampled_punctate_p1e4_peaks.narrowPeak
symbolic link to chipseq/Rad21_reps_merge_240min_downsampled_punctate_p1e4_peaks.narrowPeak

Note: generated intermediary peakcall numbers from which final union tables were generated from Blobel lab separately.  Final numbers in paper based on their union table.


Figure4FG

plot chr2:44700000-45100000 and chr2:18400000-19400000 merged kr + qnormed. merged with timpoint merged .bw from chipseq directory used. The .bw used are linked as well in output/Figure3ABCDJL.  Refer to absolute path of heatmaps in shell scripts

sh plot_chr_merged_loops_submission2_locus3.sh
sh plot_chr_merged_loops_submission2_locus4.sh 

output in output/Figure4FG


Figure5CFI

plot chr7:16900000-17300000, chr13:52800000-53200000, and chr14:79700000-80200000 with merged kr + qnormed.  Refer to absolute path of heatmaps in shell scripts.

sh plot_chr_merged_gene_submission2_locus12.sh
sh plot_chr_merged_gene_submission2_locus32.sh
sh plot_chr_merged_gene_submission2_locus33.sh

output in output/Figure5CFI

(Note: directionality index pileup in figure panel B covered under DI README)
FigureS4AF

IS bedgraph generation and IS pilups.  I link to preprocess_hic/*MERGED*, preprocess_hic/*replicate1* preprocess_hic/*replicate2* from the input subdirectory.  (Note: generate multiple copies of bedgraphs - logIScorrespond240min_*bedgraph are same, logIScorrespond120min_*bedgraph are same, logIScorrespond60min_*bedgraph are same, logIScorrespond25min_*bedgraph are same, and logIScorrespond0min_*bedgraph are same.  Essentially regenerating IS timepoint heatmap for comparing timepoint boundaries against).  These pileups *.png regenerated from Blobel lab end, but *.bedgraphs used.   

sh insulation_scorev3_boundary.sh
sh insulation_scorev3_boundary_mean.sh

output in output/FigureS4AF

total unique boundaries file stored through 240min in Figure2DEFG (also stored in output/HSVM/variance_thresholded_communities/merged/ for unique boundaries up through each timepoint)


FigureS4DE
pro-meta and early-G1 plots with domains. plot chr8:113000000-114000000 and chr9:72000000-73000000 across all timepoints for replicate1, replicate2, and merged for kr +qnormed. 

sh plot_loci_merged_cc_submission2_locus7.sh
sh plot_loci_merged_cc_submission2_locus8.sh

output in output/FigureS4DE

FigureS4I

total unique boundaries file stored through 240min in Figure2DEFG (also stored in output/HSVM/variance_thresholded_communities/merged/ for unique boundaries up through each timepoint)
IS bedgraph generation in FigureS4AF


FigureS4H

plot chr2:127500000-129500000 with domains on replicate1, replicate2, and merged for kr +qnormed.

sh plot_loci_merged_cc_submission2_locus1.sh

output in output/FigureS4H


FigureS6ABCEFGH
 
refer to output/Figure3ABCDJL for intermediary peakcalls and .bw file symbolic link.  Note: generated intermediary peakcall numbers from which final union tables were generated from Blobel lab separately.  Final numbers in paper based on their union table.


FigureS7DH

plot chr1:169200000-169700000, chr1:90200000-90800000,and chr2:47500000-49000000 across merged, replicate1, and replicate2 for kr+ qnorm across timepoints.  Merged .bw chipseq for Rad21 and CTCF plotted below heatmaps from same chipseq directory (also linked in output/Figure3ABCDJL)

sh plot_chr_merged_loops_submission2_locus5.sh
sh plot_chr_merged_loops_submission2_locus6.sh
sh plot_chr_merged_loops_submission2_locus7.sh

output in output/FigureS7DH

FigureS8BCDE

plot chr2:12750000-14750000, chr1:130500000-132500000, and chr10:118200000-118800000 for merged, replicate1, and replicate2 for kr + qnorm across timepoints.  Merged .bw chipseq for Rad21 and CTCF plotted below heatmaps from same chipseq directory (also linked in output/Figure3ABCDJL)

sh plot_chr_merged_stripes_submission2_locus26.sh
sh plot_chr_merged_stripes_submission2_locus27.sh
sh plot_chr_merged_stripes_submission2_locus28.sh

output in output/FigureS8BCDE

FigureS9BCDEGHI

plot chr1:43450000-43650000, chr14:27250000-27650000, and chr16:93750000-94100000 for merged, replicate1, and replicate2 for kr + qnorm across timepoints. Merged .bw chipseq for Rad21 and CTCF plotted below heatmaps from same chipseq directory (also linked in output/Figure3ABCDJL)

sh plot_chr_merged_loops_submission2_locus25.sh
sh plot_chr_merged_loops_submission2_locus9.sh
sh plot_chr_merged_loops_submission2_locus10.sh

prior run from output/Figure4FG/
sh plot_chr_merged_loops_submission2_locus4.sh

output in output/FigureS9BCDEGHI

FigureS10B

total unique boundaries from emergent domains provided in output/Figure2DEFG - Merged_Communities_genomewide240min10kbmerge*refine_LDv3_ex240_adjust2_b.txt (copied over from other subdirectory output/HSVM/variance_thresholded_communities/merged/)


