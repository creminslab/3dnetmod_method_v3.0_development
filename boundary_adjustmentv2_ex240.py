from read_settings import read_settings
from load_intervals import load_emergent
from create_tags import create_tags
from operator import itemgetter
import argparse
import numpy as np
import glob



def load_calls(args,tag_HSVM):
  
    #sort times and compare calls at current time to previous.  If found in previous (+- 6 bins at boundaries or gaps between boundary comparison < 10%) assign previous time. 
   
    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    community_list = []
    original_exp = args.exp   


    community_list_240min_compare = {}
    tag_HSVM_240min_compare = tag_HSVM.replace(args.exp,'240min')

    print "testing file pathway: "    
    file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '_twice_refine_LDv3_ex240.txt')[0]
    print file
    community_list  = load_emergent(file)

    return community_list

def test_uniqueness(community,unique_boundaries,collection,buffer_genome):

    #main idea is that if matchup is found between community start or end boundary (within thresholds), corresponding community boundary will be reassigned
    #to original unique boundary (principal) in list.  This value serves as a key to list of boundaries that were found to be within threshold of principal
    #from all boundaries.  In later function, the actual final boundary for community will use the key to access list and take mean of boundary list, to 
    #which the actual boundary will be assigned 


    notkey_start = True
    notkey_end = True

    community_size = np.abs(community['start'] - community['end'])

    for i in range(len(unique_boundaries)): 
   

        unique_size = np.abs(unique_boundaries[i]['end'] - unique_boundaries[i]['start'])

        #test if call lines up perfectly with call already present.  Tests if gaps between boundaries are <= 10% of call size present or both boundaries are within 6 bins of call size present. 
        #Should not be used since classify_domains_compro_refine should handle this        
        if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome and np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
            return [],unique_boundaries,collection,notkey_start,notkey_end
        elif float(np.abs(community['start'] - unique_boundaries[i]['start']) + np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.1:
            return [],unique_boundaries,collection,notkey_start,notkey_end

        #after testing redundancy only adjust right or left boundary of call (if correspond to previous call)
        #compare start to end
        if np.abs(community['start'] - unique_boundaries[i]['end']) <= buffer_genome:
            #if start is <= 6 bins from boundary in list, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['end']].append(community['start'])
            community['start'] = unique_boundaries[i]['end']
            notkey_start = False
            break
        elif (float(np.abs(community['start'] - unique_boundaries[i]['end']))/float(community_size) <= 0.075) and (float(np.abs(community['start'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.075):
            #if boundary gap is <= 10% of domain tested, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['end']].append(community['start'])
            community['start'] = unique_boundaries[i]['end']
            notkey_start = False
            break

        #compare start to start
        if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome:
            #if start is <= 6 bins from boundary in list, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['start']].append(community['start'])
            community['start'] = unique_boundaries[i]['start']
            notkey_start = False
            break
        elif (float(np.abs(community['start'] - unique_boundaries[i]['start']))/float(community_size) <= 0.075) and (float(np.abs(community['start'] - unique_boundaries[i]['start']))/float(unique_size) <= 0.075): 
            #if boundary gap is <= 10% of domain tested, readjust start
            #store list of boundaries that correspond to principle boundary
            collection[unique_boundaries[i]['start']].append(community['start'])
            community['start'] = unique_boundaries[i]['start']
            notkey_start = False
            break


    for i in range(len(unique_boundaries)):

         unique_size = np.abs(unique_boundaries[i]['end'] - unique_boundaries[i]['start'])

         if np.abs(community['start'] - unique_boundaries[i]['start']) <= buffer_genome and np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
             return [],unique_boundaries,collection,notkey_start,notkey_end
         elif float(np.abs(community['start'] - unique_boundaries[i]['start']) + np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size)  <= 0.1:
             return [],unique_boundaries,collection,notkey_start,notkey_end

         #after testing redundancy only adjust right or left boundary of call (if correspond to previous call)
         #compare end to end
         if np.abs(community['end'] - unique_boundaries[i]['end']) <= buffer_genome:
             #if start is <= 6 bins from boundary in list, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['end']].append(community['end'])
             community['end'] = unique_boundaries[i]['end']
             notkey_end = False
             break
         elif (float(np.abs(community['end'] - unique_boundaries[i]['end']))/float(community_size) <= 0.075) and (float(np.abs(community['end'] - unique_boundaries[i]['end']))/float(unique_size) <= 0.075):
             #if boundary gap is <= 10% of domain tested, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['end']].append(community['end'])
             community['end'] = unique_boundaries[i]['end']
             notkey_end = False
             break

         #compare end to start
         if np.abs(community['end'] - unique_boundaries[i]['start']) <= buffer_genome:
             #if start is <= 6 bins from boundary in list, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['start']].append(community['end'])
             community['end'] = unique_boundaries[i]['start']
             notkey_end = False
             break
         elif (float(np.abs(community['end'] - unique_boundaries[i]['start']))/float(community_size) <= 0.075) and (float(np.abs(community['end'] - unique_boundaries[i]['start']))/float(unique_size) <= 0.075) :
             #if boundary gap is <= 10% of domain tested, readjust start
             #store list of boundaries that correspond to principle boundary
             collection[unique_boundaries[i]['start']].append(community['end'])
             community['end'] = unique_boundaries[i]['start']
             notkey_end = False
             break

    #remove communities that map to same coordinates at start and end
    if community['end']  == community['start']:
        print "adjusted community to same boundary at start and end!"
        print "removing community from consideration"
        print "adjusted to unique start: ",community['start']
        print "adjusted to unique end: ", community['end']
        print "collection size at start before: ",len(collection[community['start']])    
        print "collection size at end before: ",len(collection[community['end']]) 
    
        del collection[community['end']][-1]
        del collection[community['start']][-1]
    
        print "purged original start and end from collection"
        print "collection size at start after: ",len(collection[community['start']]) 
        print "collection size at end after: ",len(collection[community['end']]) 
        community = []

    return (community,unique_boundaries,collection,notkey_start,notkey_end)

def unique_difference(collection,args):

    difference_total = []

    #collect difference from mean in collection
    for principal in collection:
        
        mean_boundary = int(np.floor(np.mean(collection[principal])/args.resolution))*args.resolution

        for i in range(len(collection[principal])):
            difference = np.abs(mean_boundary - collection[principal][i])
            difference_total.append((mean_boundary,collection[principal][i],difference, len(collection[principal]),principal))

    return difference_total


def adjust_unique(unique_boundaries,collection,args):

    #go back to collection and find start and end key that correspond to unique start and end and adjust to average of list 
    mark_removal = []
    final_unique = []
    for i in range(len(unique_boundaries)):

        start = int(np.floor(np.mean(collection[unique_boundaries[i]['start']])/args.resolution))*args.resolution
        end = int(np.floor(np.mean(collection[unique_boundaries[i]['end']])/args.resolution))*args.resolution

        difference_start = np.abs(start - unique_boundaries[i]['start'])
        difference_start = np.abs(end - unique_boundaries[i]['end'])

        unique_boundaries[i]['start'] = start #np.mean(collection[unique_boundaries[i]['start']])
        unique_boundaries[i]['end'] = end #np.mean(collection[unique_boundaries[i]['end']])
        if start == end:
            mark_removal.append(i)

    for i in range(len(unique_boundaries)):
        if i not in mark_removal:
            final_unique.append(unique_boundaries[i])

    return final_unique

def boundary_assessment(community_list,buffer_genome,times,args):

    comparison = 0
    collection = {}
    unique_boundaries = []
    non_redundant = []

    for i in range(len(times)):
        for j in range(len(community_list)):
            if times[i] != 'mitosis0min':
                comparison = int(times[i][:-3])
            if comparison == community_list[j]['time']:
                if len(collection) == 0:
                    collection[community_list[j]['start']] = []
                    collection[community_list[j]['end']] = []
                    collection[community_list[j]['start']].append(community_list[j]['start'])
                    collection[community_list[j]['end']].append(community_list[j]['end'])
                    unique_boundaries.append(community_list[j])
                else:
                    #compare boundary to list of current unique boundaries
                    output = test_uniqueness(community_list[j],unique_boundaries,collection,buffer_genome)

                    adjusted_community = output[0]
                    unique_boundaries = output[1]
                    collection = output[2]
                    notkey_start = output[3]
                    notkey_end = output[4]

                    if len(adjusted_community) > 0:
                        if (adjusted_community['chrom'],adjusted_community['start'],adjusted_community['end']) not in non_redundant:
                            unique_boundaries.append(adjusted_community)
                            non_redundant.append((adjusted_community['chrom'],adjusted_community['start'],adjusted_community['end']))
                        #print "adjusted_community",adjusted_community
                        if notkey_start:
                            collection[adjusted_community['start']] = []
                            collection[adjusted_community['start']].append(adjusted_community['start'])
                        if notkey_end:
                            collection[adjusted_community['end']] = []
                            collection[adjusted_community['end']].append(adjusted_community['end'])

    statistics = unique_difference(collection,args)
    unique_boundaries = adjust_unique(unique_boundaries,collection,args)

    return unique_boundaries,statistics                        
    
def final_calls(args,tag_HSVM,unique_boundaries,times):

    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    storage = []


    #produce output for each successive time with previous calls added
    for i in range(len(times)):

        print "testing file pathway: "
        file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '_twice_refine_LDv3_ex240.txt')[0]

        if times[i] == 'mitosis0min':
            time_instance = '0min'
            file = file.replace(args.exp,time_instance)
        else:
            time_instance = times[i]
            file = file.replace(args.exp,time_instance)        

        output = open(file[:-4] + '_adjust2.txt', 'w')

        #temp = '%s\t%s\t%s\t%s\t%s\t%s'%('#chr','start','end','left_var','right_var','time_point') 
        #print>>output,temp

        
        for j in range(len(unique_boundaries)):
            if int(unique_boundaries[j]['time']) == int(time_instance[:-3]):
                chrom = unique_boundaries[j]['chrom']
                start = unique_boundaries[j]['start']
                end = unique_boundaries[j]['end']
                left_var = unique_boundaries[j]['left_var']
                right_var = unique_boundaries[j]['right_var']
                time_value = unique_boundaries[j]['time']
                encounters = unique_boundaries[j]['occurence']
                storage_value = unique_boundaries[j]['process']

                storage.append((chrom,start,end,left_var,right_var,time_value,encounters,storage_value))

        for j in range(len(storage)):
            temp =  '%s\t%d\t%d\t%f\t%f\t%d\t%d\t%s'%(storage[j][0],storage[j][1],storage[j][2],storage[j][3],storage[j][4],storage[j][5],storage[j][6],storage[j][7])
            print>>output,temp

        output.close()
    
def final_boundaries(args,tag_HSVM,unique_boundaries,times):

    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    boundaries = []
    storage = []


    #produce output for each successive time with previous calls added
    for i in range(len(times)):

        print "testing file pathway: "
        file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '_twice_refine_LDv3_ex240.txt')[0]

        if times[i] == 'mitosis0min':
            time_instance = '0min'
            file = file.replace(args.exp,time_instance)
        else:
            time_instance = times[i]
            file = file.replace(args.exp,time_instance)    

        output = open(file[:-4] + '_adjust2_b.txt', 'w')

        #temp = '%s\t%s\t%s'%('#chr','boundary','time_point') 
        #print>>output,temp

        for j in range(len(unique_boundaries)):
            if int(unique_boundaries[j]['time']) == int(time_instance[:-3]):
                chrom = unique_boundaries[j]['chrom']
                start = unique_boundaries[j]['start']
                end = unique_boundaries[j]['end']
                left_var = unique_boundaries[j]['left_var']
                right_var = unique_boundaries[j]['right_var']
                time_value = unique_boundaries[j]['time']
                encounters = unique_boundaries[j]['occurence']
                storage_value = unique_boundaries[j]['process']

                if start not in boundaries:
                    boundaries.append(start)
                    storage.append((chrom,start,time_value))
                if end not in boundaries:
                    boundaries.append(end)
                    storage.append((chrom,end,time_value))

        for j in range(len(storage)):
            temp =  '%s\t%d\t%d'%(storage[j][0],storage[j][1],storage[j][2])
            print>>output,temp

        output.close()

def print_statistics(args,tag_HSVM,statistics):

    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '_twice_refine_LDv3_ex240.txt')[0]
    file = file.replace(args.exp,'240min')  #difference data structure does not keep track of time (key = principal, list of boundaries)

    output = open(file[:-4] + '_adjust2_difference.txt', 'w')

    temp = '%s\t%s\t%s\t%s\t%s\t%s'%('#chr','mean_adjusted','original_boundary','difference','boundaries','principal')
    print>>output,temp      

    non_zero_diff =[]
    total_diff = []

    for j in range(len(statistics)):
        temp =  '%s\t%d\t%d\t%d\t%d\t%d'%(args.chr,statistics[j][0],statistics[j][1],statistics[j][2],statistics[j][3],statistics[j][4])
        print>>output,temp       
        if statistics[j][2] > 0:
            non_zero_diff.append(statistics[j][2])
        total_diff.append(statistics[j][2])

    output.close()

    print "nonzero diff mean: ",np.mean(non_zero_diff)
    print "total diff mean: ", np.mean(total_diff)
    print "nonzero diff var: ",np.var(non_zero_diff)
    print "total diff var: ", np.var(total_diff)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('chr', type=str, help = "chromosome")
    parser.add_argument('exp',type=str, help = "experimental timepoint")
    parser.add_argument('resolution',type=int, help = "resolution")
    parser.add_argument('replicate', type=str, help="replicate")
    parser.add_argument('buffer', type=int, help="buffer around boundaries")
    parser.add_argument('settings_file',type=str,help="settings_file")

    args = parser.parse_args()

    tags = create_tags(args.settings_file)
    tag_HSVM = tags[3]
    
    times = ['mitosis0min','25min','60min','120min','240min']
    buffer_genome = args.buffer*args.resolution
    buffer_genome_2 = ((args.buffer/2)+1)*args.resolution

    community_list = load_calls(args,tag_HSVM)
    unique_boundaries,statistics = boundary_assessment(community_list,buffer_genome,times,args)
    final_calls(args,tag_HSVM,unique_boundaries,times)
    final_boundaries(args,tag_HSVM,unique_boundaries,times)
    print_statistics(args,tag_HSVM,statistics)


if __name__ == '__main__':
    main()

