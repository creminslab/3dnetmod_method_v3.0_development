#from lib5c.parsers.bed import load_features
from read_settings import read_settings
from load_intervals import load_stripped_intervals
from create_tags import create_tags
from operator import itemgetter
import argparse
import numpy as np
import glob



def assign_times(args,buffer_genome,tag_HSVM,times):
  
    #sort times and compare calls at current time to previous.  If found in previous (+- 6 bins at boundaries or gaps between boundary comparison < 10%) assign previous time. 

    dir = 'output/HSVM/variance_thresholded_communities/merged/'

    community_list = {}
    lowest_time = {}
    original_exp = args.exp   

    #iterate through times per chr
    for i in range(len(times)):

        if times[i] == 'mitosis0min':
            lowest_time['0min'] = []
        else:
            lowest_time[times[i]] = []

        if times[i] != 'mitosis0min':
            tag_HSVM = tag_HSVM.replace(args.exp,times[i])
            args.exp = times[i]

        list_HSVM = tag_HSVM.split('_')
        par_removal = list_HSVM[12]

        tag_HSVM = tag_HSVM.replace(par_removal,'*')

        print "testing file pathway: "
        print dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '.txt'
        file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '.txt')[0]

        if times[i] == 'mitosis0min':
            community_list['0min'] = {}
            community_list['0min'][args.chr]  = load_stripped_intervals(file,test=5)
        else:
            community_list[times[i]] = {}
            community_list[times[i]][args.chr]  = load_stripped_intervals(file,test=5)

        #assign instance of one to every encounter

        if times[i] == 'mitosis0min':  #iso previous timepoint for comparison so automatically add to dict and assign 0 to domain call
            for j in range(len(community_list['0min'][args.chr])):
                start = community_list['0min'][args.chr][j]['start']
                end = community_list['0min'][args.chr][j]['end']
                chrom = community_list['0min'][args.chr][j]['chrom']
                left_var = community_list['0min'][args.chr][j]['left_var']
                right_var = community_list['0min'][args.chr][j]['right_var']

                lowest_time['0min'].append([start - buffer_genome,start + buffer_genome,end - buffer_genome,end + buffer_genome,0,1,chrom,left_var,right_var])
        else: #test for existence at prior timepoint  and either assign time from previous timepoint or assign time from current time point
            for j in range(len(community_list[times[i]][args.chr])):
                start = community_list[times[i]][args.chr][j]['start']
                end = community_list[times[i]][args.chr][j]['end']
                chrom = community_list[times[i]][args.chr][j]['chrom']
                left_var = community_list[times[i]][args.chr][j]['left_var']
                right_var = community_list[times[i]][args.chr][j]['right_var']
                value_found = False
                lowest_time_value = int(times[i][:-3]) #start off with current time

                times_compare_keys = lowest_time.keys()
                for q in range(len(times_compare_keys)): #only looks at times up to current
                    times_compare_keys[q] = int(times_compare_keys[q][:-3])

                times_compare_keys.sort()

                #test for equivalent domains at prior timepoint
                for times_compare_no in times_compare_keys:
                    time_compare = str(times_compare_no) + 'min'
                    for k in range(len(lowest_time[time_compare])):
                        if (lowest_time[time_compare][k][0] <= start <= lowest_time[time_compare][k][1]) and (lowest_time[time_compare][k][2] <= end <= lowest_time[time_compare][k][3]):
                                if str(lowest_time[time_compare][k][4]) + 'min' == time_compare: #only compare to original instance (prevent boundary migration)
                                    value_found = True
                                    lowest_time_value = int(lowest_time[time_compare][k][4]) #change to lowest time found 
                                    break
                        #if outer gaps between old call and current <= 10%
                        elif float(np.abs(start - lowest_time[time_compare][k][0]) + np.abs(end - lowest_time[time_compare][k][3]))/float(end - start) <= 0.1:
                            if str(lowest_time[time_compare][k][4]) + 'min' == time_compare: #only compare to original instance (prevent boundary migration)
                                value_found = True
                                #lowest_time[time_compare][k][0] = np.floor(lowest_time[time_compare][k][0] + start)/2
                                #lowest_time[time_compare][k][3] = np.floor(lowest_time[time_compare][k][3] + end)/2 
                                lowest_time_value = int(lowest_time[time_compare][k][4]) #change to lowest time found
                                break
                        #if inner gaps between old call and curent <= 10%
                        elif float(np.abs(start - lowest_time[time_compare][k][1]) + np.abs(end - lowest_time[time_compare][k][2]))/float(end - start) <= 0.1:
                            if str(lowest_time[time_compare][k][4]) + 'min' == time_compare: #only compare to original instance (prevent boundary migration)
                                value_found = True
                                #lowest_time[time_compare][k][0] = np.floor(lowest_time[time_compare][k][0] + start)/2
                                #lowest_time[time_compare][k][3] = np.floor(lowest_time[time_compare][k][3] + end)/2
                                lowest_time_value = int(lowest_time[time_compare][k][4]) #change to lowest time found
                                break
                        if value_found:
                            break

                lowest_time[times[i]].append([start - buffer_genome,start + buffer_genome,end - buffer_genome,end + buffer_genome,lowest_time_value,1,chrom,left_var,right_var])

    args.exp = original_exp

    return lowest_time

def collect_times(args,buffer_genome,times,lowest_time):

    original_exp = args.exp

    storage = []
    #collect total number of encounters
    for i in range(len(times)):

        if times[i] == 'mitosis0min':
             times_instance = '0min'
        else:
             times_instance = times[i]

        #sort time keys to compare (all except current)
        times_compare_keys_pre= lowest_time.keys()
        times_compare_keys = []
        for q in range(len(times_compare_keys_pre)):
            if times_compare_keys_pre[q] != times_instance:
                times_compare_keys.append(int(times_compare_keys_pre[q][:-3]))

        times_compare_keys.sort()

        #look at each community for particular time...
        for j in range(len(lowest_time[times_instance])):

            #get back orignal time
            start = lowest_time[times_instance][j][0] + buffer_genome
            end = lowest_time[times_instance][j][2] + buffer_genome

            #compare to all other times
            for compare_time_int in times_compare_keys:
                time_compare = str(compare_time_int) + 'min'

                #select community from other time instance...
                for k in range(len(lowest_time[time_compare])):
                     if (lowest_time[time_compare][k][0] <= start <= lowest_time[time_compare][k][1]) and (lowest_time[time_compare][k][2] <= end <= lowest_time[time_compare][k][3]):
                             #increment by 1 for every occurence of domain at other timepoints
                             if str(lowest_time[time_compare][k][4]) + 'min' == times_instance:
                                 lowest_time[times_instance][j][5] = lowest_time[times_instance][j][5] + 1
                                 break #only increment once per timepoints
                     elif float(np.abs(start - lowest_time[time_compare][k][0]) + np.abs(end - lowest_time[time_compare][k][3]))/float(end - start) <= 0.1:  
                         if str(lowest_time[time_compare][k][4]) + 'min' == times_instance:
                             lowest_time[times_instance][j][5] = lowest_time[times_instance][j][5] + 1
                             break #only increment once per timepoints
                     elif float(np.abs(start - lowest_time[time_compare][k][1]) + np.abs(end - lowest_time[time_compare][k][2]))/float(end - start) <= 0.1:
                         if str(lowest_time[time_compare][k][4]) + 'min' == times_instance: #only compare to original instance (prevent boundary migration)
                             lowest_time[times_instance][j][5] = lowest_time[times_instance][j][5] + 1 #change to lowest time found
                             break
            #exclude 240min from final list by setting to 1
            if times_instance == '240min':
                if str(lowest_time[times_instance][j][4]) + 'min' == times_instance:
                    lowest_time[times_instance][j][5] = 1

            if (lowest_time[times_instance][j][5] >= 2): #must be more than 1 occurences 
                if str(lowest_time[times_instance][j][4]) + 'min' == times_instance: #don't add noisy calls that correspond to same point at later times
                    left_var = lowest_time[times_instance][j][7]
                    right_var = lowest_time[times_instance][j][8]
                    chrom = lowest_time[times_instance][j][6]
                    #add to storage for future timepoint usage (show previous emerge)
                    storage.append([chrom,start,end,left_var,right_var,lowest_time[times_instance][j][4],lowest_time[times_instance][j][5],'storage'])
                    


    args.exp = original_exp 

    return storage

def refine_final_calls(times,storage,buffer_genome_2,args,tag_HSVM):

    collect_time = {}
    repository = {}
    sorted_domain = {}
    storage_refine = []
   
    original_exp = args.exp

    for i in range(len(times)):

        if times[i] != 'mitosis0min':
            tag_HSVM = tag_HSVM.replace(args.exp,times[i])
            args.exp = times[i]

        list_HSVM = tag_HSVM.split('_')
        par_removal = list_HSVM[12]

        tag_HSVM = tag_HSVM.replace(par_removal,'*')

        dir = 'output/HSVM/variance_thresholded_communities/merged/'

        print "testing file pathway: "    
        print dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '.txt'
        file = glob.glob(dir + 'Merged_Communities_' + args.chr + '*mitosis' + args.exp + '*' + args.replicate + '*' + tag_HSVM + '.txt')[0]
        output = open(file[:-4] + '_twice_refine_LDv3_ex240'  + '.txt', 'w')
        print "output: ",file[:-4] + '_twice_refine_LDv3_ex240' + '.txt'

        if times[i] == 'mitosis0min':
             times_instance = '0min'
        else:
             times_instance = times[i]

        collect_time[times_instance] = []
        repository[times_instance] = []
        sorted_domain[times_instance] = []

        print "storage size = : ", len(storage)
        for j in range(len(storage)):   
            print storage[j][5]         
            if str(storage[j][5]) + 'min' == times_instance:  
                collect_time[times_instance].append([storage[j][0],storage[j][1],storage[j][2],storage[j][3],storage[j][4],storage[j][5],storage[j][6],storage[j][7]])
        
        #final cleanup of calls at time point
        print "collect_time = : ", len(collect_time[times_instance])
        for j in range(len(collect_time[times_instance])): #rearrange to make sure consideration is based on earliest coord to latest
            sorted_domain[times_instance].append((collect_time[times_instance][j][1],j))

        sorted_domain[times_instance] = sorted(sorted_domain[times_instance],key=itemgetter(0))
 
        print "sorted_domain = : ", len(sorted_domain[times_instance])
        for j in range(len(sorted_domain[times_instance])):
            chrom = collect_time[times_instance][sorted_domain[times_instance][j][1]][0]
            start = collect_time[times_instance][sorted_domain[times_instance][j][1]][1]
            end = collect_time[times_instance][sorted_domain[times_instance][j][1]][2]
            left_var = collect_time[times_instance][sorted_domain[times_instance][j][1]][3]
            right_var = collect_time[times_instance][sorted_domain[times_instance][j][1]][4]
            time_value = collect_time[times_instance][sorted_domain[times_instance][j][1]][5]
            encounters = collect_time[times_instance][sorted_domain[times_instance][j][1]][6]
            storage_value = collect_time[times_instance][sorted_domain[times_instance][j][1]][7]
            not_found = True #add to reposiory if not found
  
            if len(repository[times_instance]) == 0:
                repository[times_instance].append([chrom, start - buffer_genome_2,start + buffer_genome_2,end - buffer_genome_2,end + buffer_genome_2,left_var,right_var,time_value,encounters,'processed'])
                continue 
                
            for k in range(len(repository[times_instance])):
                if repository[times_instance][k][1] <= start <= repository[times_instance][k][2]:
                    if repository[times_instance][k][3] <= end <= repository[times_instance][k][4]:
                        not_found = False

                        consideration_start_1 = start - buffer_genome_2
                        consideration_start_2 = start + buffer_genome_2
                        consideration_end_1 = end - buffer_genome_2
                        consideration_end_2 = end + buffer_genome_2

                        if consideration_start_1 < repository[times_instance][k][1]:
                            repository[times_instance][k][1] = consideration_start_1 
                        if consideration_start_2 > repository[times_instance][k][2]:
                            repository[times_instance][k][2] = consideration_start_2
                        if consideration_end_1 < repository[times_instance][k][3]:
                            repository[times_instance][k][3] = consideration_end_1
                        if consideration_end_2 > repository[times_instance][k][4]:
                            repository[times_instance][k][4] = consideration_end_2

                        repository[times_instance][k][5] = np.max([left_var,repository[times_instance][k][5]])
                        repository[times_instance][k][6] = np.max([right_var,repository[times_instance][k][6]])
                        break
            if not_found:
                repository[times_instance].append([chrom, start - buffer_genome_2,start + buffer_genome_2,end - buffer_genome_2,end + buffer_genome_2,left_var,right_var,time_value,encounters,'processed'])


        temp = '%s\t%s\t%s\t%s\t%s\t%s'%('#chr','start','end','left_var','right_var','time_point')        #print>>output,temp
        print>>output,temp

        #output merged repository
        for j in range(len(repository[times_instance])):
            start = repository[times_instance][j][1] + buffer_genome_2
            end = repository[times_instance][j][4] - buffer_genome_2
            chrom = repository[times_instance][j][0]
            left_var = repository[times_instance][j][5]
            right_var = repository[times_instance][j][6]
            time_value = repository[times_instance][j][7]
            encounters = repository[times_instance][j][8]
            storage_value = repository[times_instance][j][9]

            temp =  '%s\t%d\t%d\t%f\t%f\t%d\t%d\t%s'%(chrom,start,end,left_var,right_var,time_value,encounters,storage_value)                    
            storage_refine.append([chrom,start,end,left_var,right_var,time_value,encounters,storage_value])

            print>>output,temp
       
        if times_instance != '0min':
           for j in range(len(storage_refine)):
               if str(storage_refine[j][5]) + 'min' != times_instance: #avoid repeating calls from current timepoint in storage
                   temp =  '%s\t%d\t%d\t%f\t%f\t%d\t%d\t%s'%(storage_refine[j][0],storage_refine[j][1],storage_refine[j][2],storage_refine[j][3],storage_refine[j][4],storage_refine[j][5],storage_refine[j][6],storage_refine[j][7])                    
                   
                   print>>output,temp

        output.close()


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('chr', type=str, help = "chromosome")
    parser.add_argument('exp',type=str, help = "experimental timepoint")
    parser.add_argument('resolution',type=int, help = "resolution")
    parser.add_argument('replicate', type=str, help="replicate")
    parser.add_argument('buffer', type=int, help="buffer around boundaries")
    parser.add_argument('settings_file',type=str,help="settings_file")

    args = parser.parse_args()

    tags = create_tags(args.settings_file)
    tag_HSVM = tags[3]
    
    times = ['mitosis0min','25min','60min','120min','240min']

    buffer_genome = args.buffer*args.resolution
    buffer_genome_2 = ((args.buffer/2)+1)*args.resolution

    lowest_time = assign_times(args,buffer_genome,tag_HSVM,times)

    storage = collect_times(args,buffer_genome,times,lowest_time)
    refine_final_calls(times,storage,buffer_genome_2,args,tag_HSVM)


if __name__ == '__main__':
    main()

