
class ComprehensiveLayering(object):

      
    """
        General stand alone class version from methods scattered about in intersections pipeline
        Initialize domains and boundaries.   
        Domains are chromosome dictionary of lists:
        [chrom,start,end,nestinglayer,bi_layer,tri_layer]
        Boundaries are chromosome dictionary of lists:
        [chrom,start_of_bin,end_of_bin,layer]

    """

    def __init__(self,domains_file,boundary_file):
        self.domains = self.load_domain(domains_file)
        self.boundaries = self.load_boundary(boundary_file) 


    def load_domain(self,domains_file):
        """ 
            must be  5 column community txt file or 3 column txt:
            chrom,start,end,left_var_right_var
                      -or-
            chrom,start,end
        """
        
        input = open(domains_file, 'r')
        domains = {}

        for line in input:
            if line.startswith('#'):
                elements = line.strip().split('\t')
                #print "number of columns: ", len(elements)
                if len(elements) != 5 and len(elements) != 3:
                   raise ValueError('did not have proper 3 or 5 col format!')
            else:
                elements = line.strip().split('\t')
                #print "number of columns: ", len(elements)
                if len(elements) != 5 and len(elements) != 3:
                   raise ValueError('did not have proper 3 or 5 col format!')
                chrom = str(line.strip().split('\t')[0])
                start = int(line.strip().split('\t')[1])
                end = int(line.strip().split('\t')[2])
                if chrom not in domains:
                    domains[chrom] = []
                domains[chrom].append([chrom,start,end,1,0,0])     

        input.close()
           
        return domains


    def load_boundary(self,boundary_file):
        """ 
            must be  3 column boundary txt file or 2 column txt:
            chrom,start_of_bin,end_of_bin
                      -or-
            chrom,start
        """

        input = open(boundary_file, 'r')
        boundaries = {}

        for line in input:
            if line.startswith('#'):
                elements = line.strip().split('\t')
                if len(elements) != 3 and len(elements) != 2:
                   raise ValueError('did not have proper 3 or 2 col format!')
            else:
                elements = line.strip().split('\t')
                if len(elements) != 3 and len(elements) != 2:
                   raise ValueError('did not have proper 3 or 2 col format!')
                chrom = str(line.strip().split('\t')[0])
                start = int(line.strip().split('\t')[1])
                if chrom not in boundaries:
                    boundaries[chrom] = []
                boundaries[chrom].append([chrom,start,0])

        input.close()
        return boundaries

    
    def assign_nesting_layers(self,layers,assignment,total_size,chrom):
        """ 
            modify nesting calls (initialized to 1) to russian nesting doll
            Adjust 4 column of self.domains to layers.  Run per chromosome
        """


        if assignment < total_size:
            for i in range(len(self.domains[chrom])):
                assign_layer = True
                if self.domains[chrom][i][3] == layers:
                    for j in range(len(self.domains[chrom])):
                        if self.domains[chrom][j][3] == layers:
                            if i != j:
                                #found layer fully nested within within do not assign it to layer yet.... 
                                if self.domains[chrom][j][1] >= self.domains[chrom][i][1] and self.domains[chrom][j][2] <= self.domains[chrom][i][2]:
                                    self.domains[chrom][i][3] = layers + 1 
                                    assign_layer = False
                                    break
                    if assign_layer: #must be inner most of all domains considered
                        assignment = assignment + 1 
                        self.domains[chrom][i][3] = layers
                        #if layers > 2:
                        #        print "layer > 2 nesting doll: ", layers

            self.assign_nesting_layers(layers+1,assignment,total_size,chrom)    

        


    def add_outer_layer(self,chrom):

        """
            bi_layer calls
            Adjust 5th column of self.domains attribute from 0 to 1 or 2.
            Assign 1 to those that have no outer, 2 to those that have outer nesting.  Outer centric.
            From the perspective of top layer - 1 means top layer (no outer nesting), 2 means internal layer (encompassing domains exist)

            per chrom basis

        """

        for i in range(len(self.domains[chrom])):
            chrom = self.domains[chrom][i][0]
            start = self.domains[chrom][i][1]
            end = self.domains[chrom][i][2]
            layer = self.domains[chrom][i][3]

            outer_layer = 1
            for j in range(len(self.domains[chrom])):
                compare_chrom = self.domains[chrom][j][0]
                compare_start = self.domains[chrom][j][1]
                compare_end = self.domains[chrom][j][2]
                compare_layer = self.domains[chrom][j][3]
                #found outer domain reassign to inner domain
                if j != i:
                    if (compare_start <= start) and (compare_end >= end):
                        outer_layer = 2
                        break

            self.domains[chrom][i][4] = outer_layer
        


    def inner_middle_outer(self):

        """
            traverse self.domains.  If encounter layer=1, already innermost (highest priority) and not reassign.  
            Otherwise, decide between middle and outer layer.  Create 3 layers - (1)innermost, (2) middle, and (3) outermost.
            Assign three layer to 6th column. WARNING: Must have nested calls assigned (4th column) first to establish layer 1.
           
        """

        for chromosome in self.domains:
            for i in range(len(self.domains[chromosome])):
                chrom = self.domains[chromosome][i][0]
                start = int(self.domains[chromosome][i][1])
                end = int(self.domains[chromosome][i][2])
                layer = int(self.domains[chromosome][i][3])
    
                if layer == 1:
                    new_layer = 1 
                else:
                    new_layer = 3 
                    for j in range(len(self.domains[chromosome])):
    
                        compare_chrom = self.domains[chromosome][j][0]
                        compare_start = int(self.domains[chromosome][j][1])
                        compare_end = int(self.domains[chromosome][j][2])
                        compare_layer = int(self.domains[chromosome][j][3])
                        #found outer domain resassign to middle
                        if compare_layer != 1:
                            if (compare_start != start) or (compare_end != end): #ensure not testing against own domain
                                if (compare_start <= start) and (compare_end >= end): 
                                    new_layer = 2 
                                    break


                self.domains[chromosome][i][5] = new_layer



    def binary_boundary_outer_inclusive(self):

        """ 
            Classify boundaries into 2 classes: those that are shared by outermost and
            those not.   Use domains and boundaries attributes as input.  self.domains 5th column
            binary outer centric calls used to reassign self.boundaries 4th column classification.
            Inner domain centric - boundaries that do not go to outer layer are 1, 2 otherwise.
            WARNING: edge case where single layer which is layer 1 assignment assigned to a layer 2
            outer.
        """
        boundary_outer_list = {}

        # iterate twice.  Find outer boundaries first.  Then iterate again
        # to fill in boundaries not shared by outer layer
        for chromosome in self.domains:
            if chromosome not in boundary_outer_list:
                boundary_outer_list[chromosome] = []
            for i in range(len(self.domains[chromosome])):
                chrom = self.domains[chromosome][i][0]
                start = self.domains[chromosome][i][1]
                end = self.domains[chromosome][i][2]
                tri = self.domains[chromosome][i][3]
                binary_nesting = self.domains[chromosome][i][4]

                if binary_nesting == 1: #outermost binary
                    if start not in boundary_outer_list[chromosome]:
                        boundary_outer_list[chromosome].append(start)
                    if end not in boundary_outer_list[chromosome]:
                        boundary_outer_list[chromosome].append(end)


        for chromosome in self.boundaries:
            for i in range(len(self.boundaries[chromosome])):
                chrom = self.boundaries[chromosome][i][0]
                start = self.boundaries[chromosome][i][1]
                if start in boundary_outer_list[chromosome]:
                    self.boundaries[chromosome][i][2] = 2
                else:
                    self.boundaries[chromosome][i][2] = 1      



    

        



     

