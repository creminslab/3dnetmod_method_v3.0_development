import numpy as np
import glob
import os
from create_tags import create_tags
import scipy.sparse
import argparse
from read_settings import read_settings
from comprehensive_layering import ComprehensiveLayering

def main():

	parser = argparse.ArgumentParser()
	parser.add_argument('chr',type=str,help="genomewide/chromosome")
	parser.add_argument('domain_calls',type=str,help="domain calls after HSVM and unique boundary adjustment must also have boundaries")

	args = parser.parse_args()

	dir2 = 'output/HSVM/variance_thresholded_communities/merged/'

	chromosomes = ['chr1','chr2','chr3','chr4','chr5','chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chr20','chr21','chr22','chrX']

	total_domains_file = dir2 + args.domain_calls
	total_boundaries_file = dir2 + args.domain_calls[:-4] + '_b.txt'

	annotations = ComprehensiveLayering(total_domains_file,total_boundaries_file)

	for chrom in annotations.domains:
		annotations.domain_nesting_doll = annotations.assign_nesting_layers(1,0,len(annotations.domains[chrom]),chrom) #russian nesting doll
		annotations.domain_bi_layers = annotations.add_outer_layer(chrom)  #1 outermost no external, 2 all others
	annotations.domain_tri_layers = annotations.inner_middle_outer()   #1 = innermost, 2 middle layers, 3 outermost.  rename from older inner_outer method
	annotations.boundaries_class = annotations.binary_boundary_outer_inclusive()  #corrected boundary class version


	output = open(total_domains_file[:-4] + '_3lay.txt', 'w')

	temp = '%s\t%s\t%s\t%s\t%s'%('chrom','start','end','original_nested_layer','three_layer')
	print>>output,temp

	for chromosome in annotations.domains:
		for i in range(len(annotations.domains[chromosome])):
			chrom = annotations.domains[chromosome][i][0]
			start = annotations.domains[chromosome][i][1]
			end = annotations.domains[chromosome][i][2]
			layer = annotations.domains[chromosome][i][3]
			three_layer = annotations.domains[chromosome][i][5]
		 
			temp = '%s\t%d\t%d\t%d\t%d'%(chrom,start,end,layer,three_layer)
			print>>output,temp

	output.close()

	
	output = open(total_boundaries_file[:-4] + '_olay_corrected.txt','w')	

	temp = '%s\t%s\t%s'%('chrom','boundary','binary_layer(2_outer_1_innerex)')  #used for insulation score *corrected* boundary
	print>>output,temp

	for chromosome in annotations.boundaries:
		for i in range(len(annotations.boundaries[chromosome])):
			chrom = annotations.boundaries[chromosome][i][0]
			boundary = annotations.boundaries[chromosome][i][1]
			layer = annotations.boundaries[chromosome][i][2]
	
			temp = '%s\t%d\t%d'%(chrom,boundary,layer)
			print>>output,temp

	output.close()


if __name__ == "__main__":
	main()
